package com.bikcode.condorprueba.presentation.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.bikcode.condorprueba.data.repository.TeamsRepositoryImpl
import com.bikcode.condorprueba.presentation.ui.util.Event
import com.bikcode.condorprueba.util.RxSchedulerRule
import com.bikcode.condorprueba.util.mockEventTeam
import io.reactivex.Single
import org.junit.Test

import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.BDDMockito.*
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailViewModelTest {

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var mockRepositoryImpl: TeamsRepositoryImpl

    @Mock
    lateinit var eventObserver: Observer<Event<DetailViewModel.DetailState>>

    lateinit var detailViewModel: DetailViewModel

    @Before
    fun setUp() {
        detailViewModel = DetailViewModel(mockRepositoryImpl)
        detailViewModel.events.observeForever(eventObserver)
    }

    @Test
    fun `getNextEvents should return a list of events from a team`() {
        //Given
        val mockListEventTeam = listOf(
            mockEventTeam
        )

        val expectedResult = Single.just(mockListEventTeam)
        `when`(mockRepositoryImpl.getNextEvents("123")).thenReturn(expectedResult)
        //When
        detailViewModel.getNextEvents("123")
        //Then

        Mockito.verify(eventObserver, times(3)).onChanged(isA(Event(DetailViewModel.DetailState.ShowLoading)::class.java))
        Mockito.verify(eventObserver, times(3)).onChanged(isA(Event(DetailViewModel.DetailState.HideLoading)::class.java))
        Mockito.verify(eventObserver, times(3)).onChanged(isA(Event(DetailViewModel.DetailState.ShowListNextEvents(mockListEventTeam))::class.java))

        Mockito.verify(mockRepositoryImpl, times(1)).getNextEvents("123")
    }
}