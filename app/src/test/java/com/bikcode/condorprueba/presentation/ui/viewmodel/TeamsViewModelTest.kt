package com.bikcode.condorprueba.presentation.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.bikcode.condorprueba.data.repository.TeamsRepositoryImpl
import com.bikcode.condorprueba.presentation.ui.util.Event
import com.bikcode.condorprueba.util.RxSchedulerRule
import com.bikcode.condorprueba.util.mockTeam
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.BDDMockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TeamsViewModelTest {

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var mockRepositoryImpl: TeamsRepositoryImpl

    @Mock
    lateinit var eventObserver: Observer<Event<TeamsViewModel.TeamsState>>

    private lateinit var teamViewModel: TeamsViewModel

    @Before
    fun setUp() {
        teamViewModel = TeamsViewModel(mockRepositoryImpl)
        teamViewModel.events.observeForever(eventObserver)
    }

    @Test
    fun `getTeamsFromLeague should get a list of teams from a league successfully`() {
        //Given
        val mockTeamList = listOf(
            mockTeam
        )

        val expectedResult = Single.just(mockTeamList)

        given(mockRepositoryImpl.getAllTeamsDB(anyString())).willReturn(expectedResult)

        //When
        teamViewModel.getTeamsFromLeague(anyString())

        //Then
        Mockito.verify(eventObserver, times(0)).onChanged(isA(Event(TeamsViewModel.TeamsState.ShowError("Error"))::class.java))
        Mockito.verify(eventObserver, times(2)).onChanged(isA(Event(TeamsViewModel.TeamsState.ShowLoading)::class.java))
        Mockito.verify(eventObserver, times(2)).onChanged(isA(Event(TeamsViewModel.TeamsState.HideLoading)::class.java))

        Mockito.verify(mockRepositoryImpl, times(1)).getAllTeamsDB(anyString())
    }

    @Test
    fun `getTeamsFromLeague should return a error`() {
        //Given
        val expectedResult = Throwable("Error")

        given(mockRepositoryImpl.getAllTeamsDB(anyString())).willReturn(Single.error(expectedResult))

        //When
        teamViewModel.getTeamsFromLeague(anyString())

        Mockito.verify(eventObserver, times(3)).onChanged(isA(Event(TeamsViewModel.TeamsState.ShowLoading)::class.java))
        Mockito.verify(eventObserver, times(3)).onChanged(isA(Event(TeamsViewModel.TeamsState.HideLoading)::class.java))
        Mockito.verify(eventObserver, times(3)).onChanged(isA(Event(TeamsViewModel.TeamsState.ShowError("Error"))::class.java))

        Mockito.verify(mockRepositoryImpl, times(1)).getAllTeamsDB(anyString())
    }
}