package com.bikcode.condorprueba.util

import com.bikcode.condorprueba.data.source.remote.EventTeam
import com.bikcode.condorprueba.data.source.remote.Team
import com.bikcode.condorprueba.domain.entity.TeamEntity

val mockTeam = Team(
    1,
    2,
    "title mock",
    1995,
    "mock title stadium",
    "mock description",
    "10.0",
    "",
    "mock twitter",
    "mock instagram",
    "",
    ""
)

val mockTeamEntity = TeamEntity(
    1,
    2,
    "title mock",
    1995,
    "mock title stadium",
    "mock description",
    "10.0",
    "",
    "mock twitter",
    "mock instagram",
    "",
    ""
)

val mockEventTeam = EventTeam(
    1,
    "mock event",
    "date"
)