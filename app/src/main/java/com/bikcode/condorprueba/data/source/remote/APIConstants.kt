package com.bikcode.condorprueba.data.source.remote

object APIConstants {
    const val BASE_URL = "https://www.thesportsdb.com/api/v1/json/1/"

    //ENDPOINTS
    const val TEAMS_LEAGUE_ENDPOINT = "lookup_all_teams.php"
    const val NEXT_EVENTS_ENDPOINT = "eventsnext.php"

    //KEYS
    const val KEY_ID = "id"
    const val KEY_ID_LEAGUE = "idLeague"
    const val KEY_TEAM_ID = "idTeam"
    const val KEY_TEAMS = "teams"
    const val KEY_TEAM_NAME = "strTeam"
    const val KEY_FORMED_YEAR = "intFormedYear"
    const val KEY_STADIUM_NAME = "strStadium"
    const val KEY_TEAM_DESCRIPTION = "strDescriptionEN"
    const val KEY_WEBSITE = "strWebsite"
    const val KEY_FACEBOOK = "strFacebook"
    const val KEY_TWITTER = "strTwitter"
    const val KEY_INSTAGRAM = "strInstagram"
    const val KEY_TEAM_BADGE = "strTeamBadge"
    const val KEY_TEAM_JERSEY = "strTeamJersey"
}