package com.bikcode.condorprueba.data.source.local

import com.bikcode.condorprueba.data.source.remote.Team
import com.bikcode.condorprueba.domain.entity.TeamEntity

fun TeamEntity.toTeam() = Team(
    idTeam,
    idLeague,
    strTeam,
    intFormedYear,
    strStadium,
    strDescriptionEN,
    strWebsite,
    strFacebook,
    strTwitter,
    strInstagram,
    strTeamBadge,
    strTeamJersey
)