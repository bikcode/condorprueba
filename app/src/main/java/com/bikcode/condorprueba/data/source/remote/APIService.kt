package com.bikcode.condorprueba.data.source.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    @GET(APIConstants.TEAMS_LEAGUE_ENDPOINT)
    fun getAllTeams(
        @Query(APIConstants.KEY_ID) idLeague: String

    ): Single<TeamsResponseServer>

    @GET(APIConstants.NEXT_EVENTS_ENDPOINT)
    fun getNextEvents(@Query(APIConstants.KEY_ID) id: String): Single<EventServer>
}