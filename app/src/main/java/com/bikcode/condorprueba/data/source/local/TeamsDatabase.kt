package com.bikcode.condorprueba.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bikcode.condorprueba.domain.entity.TeamEntity

@Database(entities = [TeamEntity::class], version = 1)
abstract class TeamsDatabase : RoomDatabase(){
    abstract fun teamDao(): TeamDao

    companion object {
        const val DB_NAME = "teams-db"
    }
}