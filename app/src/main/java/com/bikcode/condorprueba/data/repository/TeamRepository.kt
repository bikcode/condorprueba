package com.bikcode.condorprueba.data.repository

import com.bikcode.condorprueba.data.source.remote.EventTeam
import com.bikcode.condorprueba.data.source.remote.Team
import com.bikcode.condorprueba.domain.entity.TeamEntity
import io.reactivex.Single

interface TeamRepository {

    fun getAllTeamsAPI(idLeague: String): Single<List<Team>>
    fun saveTeams(teams:List<TeamEntity>)
    fun getAllTeamsDB(idLeague: String): Single<List<Team>>
    fun getNextEvents(idTeam: String): Single<List<EventTeam>>
}