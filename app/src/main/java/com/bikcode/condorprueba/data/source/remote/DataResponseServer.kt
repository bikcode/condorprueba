package com.bikcode.condorprueba.data.source.remote

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TeamsResponseServer(

    @SerializedName(APIConstants.KEY_TEAMS) val teams: List<Team>
)

data class Team(
    @SerializedName(APIConstants.KEY_TEAM_ID) val idTeam: Int,
    @SerializedName(APIConstants.KEY_ID_LEAGUE) val idLeague: Int,
    @SerializedName(APIConstants.KEY_TEAM_NAME) val strTeam: String,
    @SerializedName(APIConstants.KEY_FORMED_YEAR) val intFormedYear: Int,
    @SerializedName(APIConstants.KEY_STADIUM_NAME) val strStadium: String?,
    @SerializedName(APIConstants.KEY_TEAM_DESCRIPTION) val strDescriptionEN: String,
    @SerializedName(APIConstants.KEY_WEBSITE) val strWebsite: String?,
    @SerializedName(APIConstants.KEY_FACEBOOK) val strFacebook: String?,
    @SerializedName(APIConstants.KEY_TWITTER) val strTwitter: String?,
    @SerializedName(APIConstants.KEY_INSTAGRAM) val strInstagram: String?,
    @SerializedName(APIConstants.KEY_TEAM_BADGE) val strTeamBadge: String?,
    @SerializedName(APIConstants.KEY_TEAM_JERSEY) val strTeamJersey: String?
) : Serializable

data class EventServer(

    @SerializedName("events") val events: List<EventTeam>
)

data class EventTeam(
    @SerializedName("idEvent") val idEvent: Int,
    @SerializedName("strEvent") val strEvent: String,
    @SerializedName("dateEvent") val dateEvent: String,
)