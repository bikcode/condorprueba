package com.bikcode.condorprueba.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bikcode.condorprueba.domain.entity.TeamEntity
import io.reactivex.Single

@Dao
interface TeamDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTeams(teams: List<TeamEntity>)

    @Query("SELECT * FROM team WHERE idLeague = :idLeague")
    fun getAllTeams(idLeague: String): Single<List<TeamEntity>>
}