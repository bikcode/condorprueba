package com.bikcode.condorprueba.data.repository

import com.bikcode.condorprueba.data.source.local.TeamDao
import com.bikcode.condorprueba.data.source.local.toTeam
import com.bikcode.condorprueba.data.source.remote.APIService
import com.bikcode.condorprueba.data.source.remote.EventTeam
import com.bikcode.condorprueba.data.source.remote.Team
import com.bikcode.condorprueba.data.source.remote.toTeamEntity
import com.bikcode.condorprueba.domain.entity.TeamEntity
import io.reactivex.Single

class TeamsRepositoryImpl(private val apiService: APIService, private val teamDao: TeamDao) :
    TeamRepository {
    override fun getAllTeamsAPI(idLeague: String): Single<List<Team>> {
        return apiService.getAllTeams(
            idLeague
        ).flatMap { response ->
            val teams = response.teams.map { team -> team.toTeamEntity() }
            teamDao.insertTeams(teams)
            Single.just(response.teams)
        }
    }

    override fun saveTeams(teams: List<TeamEntity>) {
        teamDao.insertTeams(teams)
    }

    override fun getAllTeamsDB(idLeague: String): Single<List<Team>> {
        return teamDao.getAllTeams(idLeague).flatMap { teams ->
            val events = teams.map { team -> team.toTeam() }
            Single.just(events)
        }
    }

    override fun getNextEvents(idTeam: String): Single<List<EventTeam>> {
        return apiService.getNextEvents(idTeam).flatMap { response ->
            val events = response.events.map { event -> event }
            Single.just(events)
        }
    }
}