package com.bikcode.condorprueba.data.source.remote

import com.bikcode.condorprueba.domain.entity.TeamEntity

fun Team.toTeamEntity() = TeamEntity(
    idTeam,
    idLeague,
    strTeam,
    intFormedYear,
    strStadium,
    strDescriptionEN,
    strWebsite,
    strFacebook,
    strTwitter,
    strInstagram,
    strTeamBadge,
    strTeamJersey
)