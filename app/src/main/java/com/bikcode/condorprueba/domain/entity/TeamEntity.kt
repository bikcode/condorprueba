package com.bikcode.condorprueba.domain.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "team")
data class TeamEntity(
    @PrimaryKey val idTeam: Int,
    val idLeague: Int,
    val strTeam: String,
    val intFormedYear: Int,
    val strStadium: String?,
    val strDescriptionEN: String,
    val strWebsite: String?,
    val strFacebook: String?,
    val strTwitter: String?,
    val strInstagram: String?,
    val strTeamBadge: String?,
    val strTeamJersey: String?
)