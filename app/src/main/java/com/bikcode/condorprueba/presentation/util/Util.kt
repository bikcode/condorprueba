package com.bikcode.condorprueba.presentation.util

import android.content.Intent
import android.net.Uri

object Util {
    fun getIntentForWeb(url: String): Intent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse(url)
    )

    const val SPAIN_LEAGUE_ID = "4335"
    const val PREMIER_LEAGUE_ID = "4328"
    const val FRENCH_LEAGUE_ID = "4334"
}