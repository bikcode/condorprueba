package com.bikcode.condorprueba.presentation.di

import com.bikcode.condorprueba.data.source.remote.APIClient
import com.bikcode.condorprueba.data.source.remote.APIService
import org.koin.dsl.module

val networkModule = module {
    single { APIClient().createWebService().create(APIService::class.java) }
}