package com.bikcode.condorprueba.presentation.di

import com.bikcode.condorprueba.data.repository.TeamRepository
import com.bikcode.condorprueba.data.repository.TeamsRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    factory { TeamsRepositoryImpl(get(), get()) }
}