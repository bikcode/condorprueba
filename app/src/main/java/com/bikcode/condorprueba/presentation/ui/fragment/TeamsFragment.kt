package com.bikcode.condorprueba.presentation.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.bikcode.condorprueba.R
import com.bikcode.condorprueba.databinding.FragmentTeamsBinding
import com.bikcode.condorprueba.presentation.ui.adapter.TeamAdapter
import com.bikcode.condorprueba.presentation.ui.util.Event
import com.bikcode.condorprueba.presentation.ui.viewmodel.TeamsViewModel
import com.bikcode.condorprueba.presentation.util.Util.FRENCH_LEAGUE_ID
import com.bikcode.condorprueba.presentation.util.Util.PREMIER_LEAGUE_ID
import com.bikcode.condorprueba.presentation.util.Util.SPAIN_LEAGUE_ID
import com.bikcode.condorprueba.presentation.util.showShortToast
import org.koin.androidx.viewmodel.ext.android.viewModel

class TeamsFragment : Fragment(R.layout.fragment_teams) {

    private val teamsViewModel: TeamsViewModel by viewModel()
    private var _binding: FragmentTeamsBinding? = null
    private val binding get() = _binding!!
    private lateinit var teamAdapter: TeamAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentTeamsBinding.bind(view)
        init()
        teamsViewModel.getTeamsFromLeague(SPAIN_LEAGUE_ID)
        teamsViewModel.events.observe(viewLifecycleOwner, this::handleEvents)
        teamsViewModel.teams.observe(viewLifecycleOwner, { teams ->
            teamAdapter.submitList(teams)
            showTeamsList()
        })
    }

    private fun init() {
        teamAdapter = TeamAdapter { team ->
            val action = TeamsFragmentDirections.actionTeamsFragmentToDetailTeamFragment(team)
            NavHostFragment.findNavController(this).navigate(action)
        }

        binding.teamsRvTeams.adapter = teamAdapter
        binding.teamsRgFilter.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.teams_rb_spain -> {
                    resetAdapter()
                    teamsViewModel.getTeamsFromLeague(SPAIN_LEAGUE_ID)
                }
                R.id.teams_rb_premier -> {
                    resetAdapter()
                    teamsViewModel.getTeamsFromLeague(PREMIER_LEAGUE_ID)
                }
                R.id.teams_rb_french -> {
                    resetAdapter()
                    teamsViewModel.getTeamsFromLeague(FRENCH_LEAGUE_ID)
                }
            }
        }
    }

    private fun resetAdapter() {
        teamAdapter.submitList(emptyList())
        binding.teamsRvTeams.isVisible = false
    }

    private fun showTeamsList() {
        binding.teamsRvTeams.isVisible = true
    }

    private fun handleEvents(events: Event<TeamsViewModel.TeamsState>?) {
        events?.getContentIfNotHandled()?.let { state ->
            when (state) {
                TeamsViewModel.TeamsState.ShowLoading -> binding.teamsPbProgress.isVisible = true
                TeamsViewModel.TeamsState.HideLoading -> binding.teamsPbProgress.isVisible = false
                is TeamsViewModel.TeamsState.ShowError -> {
                    binding.teamsGroupData.isVisible = false
                    context?.showShortToast(state.message)
                    binding.teamsTvErrorMessage.isVisible = true
                }
            }
        }
    }

    companion object {
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}