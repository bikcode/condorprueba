package com.bikcode.condorprueba.presentation.di

import com.bikcode.condorprueba.presentation.ui.viewmodel.DetailViewModel
import com.bikcode.condorprueba.presentation.ui.viewmodel.TeamsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { TeamsViewModel(get()) }
    viewModel { DetailViewModel(get()) }
}