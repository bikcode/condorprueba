package com.bikcode.condorprueba.presentation.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bikcode.condorprueba.data.repository.TeamsRepositoryImpl
import com.bikcode.condorprueba.data.source.remote.EventTeam
import com.bikcode.condorprueba.presentation.ui.util.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class DetailViewModel(private val teamsRepositoryImpl: TeamsRepositoryImpl) : ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    private val _events: MutableLiveData<Event<DetailState>> = MutableLiveData()
    val events: LiveData<Event<DetailState>>
        get() = _events

    fun getNextEvents(idTeam: String) {
        disposable.add(
            teamsRepositoryImpl.getNextEvents(idTeam)
                .doOnSubscribe { showLoading() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy({ error ->
                    hideLoading()
                    _events.value = Event(DetailState.ShowError(error.message ?: "Error"))
                }, { events ->
                    hideLoading()
                    _events.value = Event(DetailState.ShowListNextEvents(events))
                })
        )
    }

    private fun showLoading() {
        _events.postValue(Event(DetailState.ShowLoading))
    }

    private fun hideLoading() {
        _events.value = Event(DetailState.HideLoading)
    }

    sealed class DetailState {
        object ShowLoading : DetailState()
        object HideLoading : DetailState()
        data class ShowListNextEvents(val nextEvents: List<EventTeam>) : DetailState()
        data class ShowError(val message: String) : DetailState()
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }
}