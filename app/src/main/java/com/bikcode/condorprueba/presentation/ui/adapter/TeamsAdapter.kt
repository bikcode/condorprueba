package com.bikcode.condorprueba.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bikcode.condorprueba.R
import com.bikcode.condorprueba.data.source.remote.Team
import com.bikcode.condorprueba.databinding.TeamItemBinding
import com.bikcode.condorprueba.presentation.util.bindImageURL

class TeamAdapter(private val callback: (Team) -> Unit) :
    ListAdapter<Team, TeamAdapter.TeamViewHolder>(DiffUtilCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TeamAdapter.TeamViewHolder {
        val binding = TeamItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TeamViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TeamAdapter.TeamViewHolder, position: Int) {
        val team = getItem(position)
        holder.bind(team)
        holder.bindOnclick(team)
    }


    inner class TeamViewHolder(private val binding: TeamItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(team: Team) {
            binding.teamDetailTvTeamName.text = team.strTeam
            binding.teamDetailTvTeamStadium.text = team.strStadium
            binding.teamItemIvBadge.bindImageURL(
                team.strTeamBadge,
                R.drawable.ic_placeholder,
                R.drawable.ic_placeholder_error
            )
        }

        fun bindOnclick(team: Team) {
            binding.teamItemCvContainer.setOnClickListener {
                callback.invoke(team)
            }
        }
    }
}

private class DiffUtilCallback : DiffUtil.ItemCallback<Team>() {
    override fun areItemsTheSame(oldItem: Team, newItem: Team): Boolean {
        return oldItem.idTeam == newItem.idTeam
    }

    override fun areContentsTheSame(oldItem: Team, newItem: Team): Boolean {
        return oldItem == newItem
    }

}