package com.bikcode.condorprueba.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bikcode.condorprueba.data.source.remote.EventTeam
import com.bikcode.condorprueba.databinding.EventItemBinding

class NextEventsAdapter :
    ListAdapter<EventTeam, NextEventsAdapter.NextEventsViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NextEventsViewHolder {
        val binding = EventItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NextEventsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NextEventsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class NextEventsViewHolder(private val binding: EventItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(nextEvent: EventTeam) {
            binding.eventItemTvMatch.text = nextEvent.strEvent
            binding.eventItemTvDate.text = nextEvent.dateEvent
        }
    }
}

class DiffCallback : DiffUtil.ItemCallback<EventTeam>() {
    override fun areItemsTheSame(oldItem: EventTeam, newItem: EventTeam): Boolean {
        return oldItem.idEvent == newItem.idEvent
    }

    override fun areContentsTheSame(oldItem: EventTeam, newItem: EventTeam): Boolean {
        return oldItem == newItem
    }

}