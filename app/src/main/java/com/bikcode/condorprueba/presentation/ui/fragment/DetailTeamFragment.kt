package com.bikcode.condorprueba.presentation.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.bikcode.condorprueba.R
import com.bikcode.condorprueba.data.source.remote.Team
import com.bikcode.condorprueba.databinding.FragmentDetailTeamBinding
import com.bikcode.condorprueba.presentation.ui.adapter.NextEventsAdapter
import com.bikcode.condorprueba.presentation.ui.util.Event
import com.bikcode.condorprueba.presentation.ui.viewmodel.DetailViewModel
import com.bikcode.condorprueba.presentation.util.bindImageURL
import com.bikcode.condorprueba.presentation.util.showShortToast
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailTeamFragment : Fragment(R.layout.fragment_detail_team) {

    private val detailViewModel: DetailViewModel by viewModel()
    private var _binding: FragmentDetailTeamBinding? = null
    private val binding get() = _binding!!
    private val nextEventsAdapter: NextEventsAdapter by lazy { NextEventsAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentDetailTeamBinding.bind(view)
        arguments?.let {
            val team = DetailTeamFragmentArgs.fromBundle(it).teamSpecific
            setFields(team)
            init()
            detailViewModel.events.observe(viewLifecycleOwner, this::handleEvents)
            detailViewModel.getNextEvents(team.idTeam.toString())
        }
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

    private fun init() {
        binding.detailTeamRvNextEvents.adapter = nextEventsAdapter
    }

    private fun handleEvents(event: Event<DetailViewModel.DetailState>?) {
        event?.getContentIfNotHandled()?.let { state ->
            when (state) {
                DetailViewModel.DetailState.ShowLoading -> binding.detailTeamPrProgres.isVisible =
                    true
                DetailViewModel.DetailState.HideLoading -> binding.detailTeamPrProgres.isVisible =
                    false
                is DetailViewModel.DetailState.ShowListNextEvents -> {
                    if (state.nextEvents.isNotEmpty())
                        nextEventsAdapter.submitList(state.nextEvents)
                    else
                        binding.detailTeamGroupNextEvents.isVisible = false
                }
                is DetailViewModel.DetailState.ShowError -> {
                    context?.showShortToast(state.message)
                    binding.detailTeamGroupNextEvents.isVisible = false
                }
            }
        }
    }

    private fun setFields(team: Team) {
        binding.detailTeamIvBadge.bindImageURL(
            team.strTeamBadge,
            R.drawable.ic_placeholder,
            R.drawable.ic_placeholder_error
        )
        binding.detailTeamIvJersey.bindImageURL(
            team.strTeamJersey, R.drawable.ic_placeholder,
            R.drawable.ic_placeholder_error
        )
        binding.detailTeamTvTeamDescription.text = team.strDescriptionEN
        binding.detailTeamTvTeamName.text = team.strTeam
        binding.detailTeamTvTeamFormed.text = team.intFormedYear.toString()

        team.strFacebook?.let {
            binding.detailTeamIbFacebook.isVisible = true
            setOnclick(it, binding.detailTeamIbFacebook)
        }
        team.strInstagram?.let {
            binding.detailTeamIbInstagram.isVisible = true
            setOnclick(it, binding.detailTeamIbInstagram)
        }
        team.strTwitter?.let {
            binding.detailTeamIbTwitter.isVisible = true
            setOnclick(it, binding.detailTeamIbTwitter)
        }
        team.strWebsite?.let {
            binding.detailTeamIbWeb.isVisible = true
            setOnclick(it, binding.detailTeamIbWeb)
        }
    }

    private fun setOnclick(url: String, button: AppCompatImageButton) {
        var urlIntent: String? = null
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            urlIntent = "http://$url"

        button.setOnClickListener {

            activity?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(urlIntent)))
        }
    }
}