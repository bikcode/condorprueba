package com.bikcode.condorprueba.presentation.di

import androidx.room.Room
import com.bikcode.condorprueba.data.source.local.TeamsDatabase
import org.koin.dsl.module

val roomModule = module {
    single { Room.databaseBuilder(get(), TeamsDatabase::class.java, TeamsDatabase.DB_NAME).build() }
    factory { get<TeamsDatabase>().teamDao() }
}