package com.bikcode.condorprueba.presentation.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bikcode.condorprueba.data.repository.TeamsRepositoryImpl
import com.bikcode.condorprueba.data.source.remote.Team
import com.bikcode.condorprueba.presentation.ui.util.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class TeamsViewModel(private val teamsRepositoryImpl: TeamsRepositoryImpl) : ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    private val _teams: MutableLiveData<List<Team>> = MutableLiveData()
    val teams: LiveData<List<Team>>
        get() = _teams

    private val _events: MutableLiveData<Event<TeamsState>> = MutableLiveData()
    val events: LiveData<Event<TeamsState>>
        get() = _events

    fun getTeamsFromLeague(idLeague: String) {
        disposable.add(
            teamsRepositoryImpl.getAllTeamsDB(idLeague)
                .doOnSubscribe {
                    showLoading()
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy({ error ->
                    _events.value = Event(TeamsState.ShowError(error.message ?: "Error"))
                    hideLoading()
                }, { teamsData ->
                    hideLoading()
                    if(teamsData.isEmpty())
                        getTeamsAPI(idLeague)
                    else
                        _teams.value = teamsData
                })
        )
    }

    private fun getTeamsAPI(idLeague: String) {
        disposable.add(
            teamsRepositoryImpl.getAllTeamsAPI(idLeague)
                .doOnSubscribe {
                    showLoading()
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy({ error ->
                    hideLoading()
                    _events.value = Event(TeamsState.ShowError(error.message ?: "Error"))
                }, { teamsData ->
                    hideLoading()
                    _teams.value = teamsData
                })
        )
    }

    private fun showLoading() {
        _events.postValue(Event(TeamsState.ShowLoading))
    }

    private fun hideLoading() {
        _events.value = Event(TeamsState.HideLoading)
    }

    sealed class TeamsState {
        object ShowLoading : TeamsState()
        object HideLoading : TeamsState()
        data class ShowError(val message: String) : TeamsState()
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }
}