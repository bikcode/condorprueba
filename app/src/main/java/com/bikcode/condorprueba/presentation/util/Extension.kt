package com.bikcode.condorprueba.presentation.util

import android.content.Context
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide

fun Context.showShortToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun AppCompatImageView.bindImageURL(
    url: String?,
    @DrawableRes placeholder: Int, @DrawableRes placeholderError: Int
) {
    if(url.isNullOrEmpty()) {
        setImageResource(placeholderError)
        return
    }

    Glide.with(context)
        .load(url)
        .placeholder(placeholder)
        .error(placeholderError)
        .into(this)
}