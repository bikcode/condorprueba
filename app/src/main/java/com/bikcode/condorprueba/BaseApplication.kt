package com.bikcode.condorprueba

import android.app.Application
import com.bikcode.condorprueba.presentation.di.networkModule
import com.bikcode.condorprueba.presentation.di.repositoryModule
import com.bikcode.condorprueba.presentation.di.roomModule
import com.bikcode.condorprueba.presentation.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BaseApplication)
            modules(
                listOf(
                    networkModule,
                    repositoryModule,
                    viewModelModule,
                    roomModule
                )
            )
        }
    }
}