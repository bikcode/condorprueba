package com.bikcode.condorprueba.source.local

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bikcode.condorprueba.data.source.local.TeamDao
import com.bikcode.condorprueba.data.source.local.TeamsDatabase
import com.bikcode.condorprueba.domain.entity.TeamEntity
import org.junit.*
import org.junit.runner.RunWith
import java.util.concurrent.Executors

@RunWith(AndroidJUnit4::class)
class TeamDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var teamDao: TeamDao
    private lateinit var db: TeamsDatabase

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, TeamsDatabase::class.java)
            .setTransactionExecutor(Executors.newSingleThreadExecutor())
            .build()
        teamDao = db.teamDao()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun should_insert_teams_and_return_teams() {
        //Given
        val teams = listOf(
            TeamEntity(
                1,
                2,
                "title mock",
                1995,
                "mock title stadium",
                "mock description",
                "10.0",
                "",
                "mock twitter",
                "mock instagram",
                "",
                ""
            )
        )

        //When
        teamDao.insertTeams(teams)
        teamDao.getAllTeams("2").test()
            .assertNoErrors()
            .assertComplete()
            .assertValueCount(1)
            .assertResult(teams)
    }

}