# Condor Prueba

La app consiste en que se consulta los equipos de una liga en especifico, por defecto se consultan los de la liga española, pero el usuario puede variar de liga y consultar los equipos y a su vez podrá ver el detalle de cada equipo.

Sobre cada item de la lista de los equipos el usuario tendrá la posibilidad de dar tap sobre el equipo preferido y será dirigido a una nueva pantalla donde podrá ver mas detalles del equipo.

### Requisitos para construir el proyecto

Clonar o descargar el proyecto

- Para clonar: git clone https://gitlab.com/bikcode/condorprueba.git
- Esperar que se sincronice el proyecto
- Compilar en un emulador o en celular smartphone con version minima de android 6.0 (Marshmallow)

### Para la realización de este proyecto se emplea:

  - **Kotlin** con arquitectura **MVVM** + patrón **repository**
  - **Retrofit** para las peticiones HTTP
  - **Room database** para el almacenamiento local 
  - **Navigation component** para tener un solo activity y las demas pantallas fragments y navegar entre ellas.
  - **Rx Java** para las peticiones asincronas y manejo de datos
  - **Koin** para la inyección de dependencias
  - **Material design** para el diseño
  - **Pruebas unitarias** para los viewmodels
  - **Prueba de implementación** para room
  - **https://www.thesportsdb.com/api.php** la api REST para la obtención de las ligas y equipos
